yameme
======

Script for generating (admittedly stupid) image memes (Scumbag Steve, Insanity Wolf, Philosoraptor etc.)

Template images are downloaded from http://imgflip.com/memetemplates

Usage
-----

     perl yameme.pl --meme MEME_NAME --top TOP_TEXT --bottom BOTTOM_TEXT --output OUTPUT_FILE

e.g.:

     perl yameme.pl --meme insanity-wolf --top 'MEMES' --bottom 'I ADORE THEM' --output output.jpg

Slashes are turned into new lines, e.g.:

     perl yameme.pl --meme insanity-wolf --top 'FIRST TOP LINE/SECOND TOP LINE' --output output.jpg

Author
------

Filip Graliński
