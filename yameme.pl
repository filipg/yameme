#!/usr/bin/perl

use strict;
use utf8;

use Image::Magick;
use LWP::Simple qw($ua getstore);
use Readonly;
use Getopt::Long;

Readonly my $CACHE_DIRECTORY => 'cache';
Readonly my $EDGE_SIZE => 20;

use Log::Log4perl qw(:easy);
Log::Log4perl->easy_init($INFO);

binmode(STDOUT, ':utf8');
binmode(STDERR, ':utf8');

my $meme;
my $top;
my $bottom;
my $output;
my $script;

my $options_okay = GetOptions(
    'meme=s' => \$meme,
    'top=s' => \$top,
    'bottom=s' => \$bottom,
    'output=s' => \$output,
    'script=s' => \$script,
    'help' => sub { help(); exit 1; },
);

if (!$options_okay) {
    help();
    exit 1;
}

$ua->agent('Yameme/1.0');

check_cache_dir();

if (defined($meme) && defined($script)) {
    die "both --meme and --script specified";
}

if (defined($script)) {
    if (! -d $output) {
        die "`$output` should be a directory";
    }

    process_script($script);
    `cp jquery.js ${output}/`;
} else {
    create_meme(
        {'meme' => $meme,
         'top' => $top,
         'bottom' => $bottom,
         'output' => $output,
     });
}

sub help {
    print <<'END_OF_HELP'
perl yameme.pl --meme MEME_NAME --top TOP_TEXT --bottom BOTTOM_TEXT --output OUTPUT_FILE

e.g.:
perl yameme.pl --meme insanity-wolf --top 'MEMES' --bottom 'I ADORE THEM' --output output.jpg

Slashes are turned into new lines, e.g.:
perl yameme.pl --meme insanity-wolf --top 'FIRST TOP LINE/SECOND TOP LINE' --output output.jpg
END_OF_HELP

}

sub check_cache_dir {
    if (! -d $CACHE_DIRECTORY) {
        INFO "Creating `$CACHE_DIRECTORY`";
        mkdir($CACHE_DIRECTORY);
    }
}

sub process_script {
    my ($script) = @_;

    my @meme_instructions = parse_script($script);

    my $meme_number = 1;

    for my $meme_instruction (@meme_instructions) {

        my $formatted_number = get_formatted_meme_number($meme_number);

        create_meme({
            'meme' => $meme_instruction->[0],
            'top' => $meme_instruction->[1],
            'bottom' => $meme_instruction->[2],
            'output' => "${output}/${formatted_number}.jpg"});

        create_html_page($meme_number);


        ++$meme_number;
    }
}

sub create_html_page {
    my ($meme_number) = @_;

    my $formatted_number = get_formatted_meme_number($meme_number);

    open my $fh, '>', "${output}/${formatted_number}.html";
    binmode($fh, ':utf8');

    my $next_link = get_formatted_meme_number($meme_number+1);
    my $prev_link = get_formatted_meme_number($meme_number == 1 ? 1 : $meme_number-1);

    print $fh <<"END_OF_HTML";
<html>
<head>
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript">

function do_it() {
\$(document.body).keydown(function(e) {
   if ( e.keyCode === 33 || e.keyCode === 34 ) {
        e.preventDefault();
       location.href = ( e.keyCode === 33 ) ? '${prev_link}.html' : '${next_link}.html'
    }
  });
}
</script>
</head>
<body onload="do_it()">
<p style="top: 50%; left:-50%"><img style="height:100%; display:block;margin-left:auto;margin-right:auto;margin-top:0;margin-bottom:0" src="${formatted_number}.jpg"></p>

</body>
</html>
END_OF_HTML

    close $fh;
}

sub get_formatted_meme_number {
    my ($meme_number) = @_;

    return sprintf("%03d", $meme_number);
}

sub parse_script {
    my ($script) = @_;

    my @instructions;

    open my $fh, '<', $script;
    binmode($fh, ':utf8');

    while (my $line = <$fh>) {
        chomp $line;
        $line =~ s/\#.*$//;
        if ($line =~ /^\s*$/) {
            ;
        } elsif (my ($meme, $top, $bottom) = ($line =~ /^\s*(\S+)\s+'([^']*)'\s+'([^']*)'\s*$/)) {
            push @instructions, [$meme, $top, $bottom];
        } else {
            die "unexpected line `$line`!!!"
        }
    }

    return @instructions;
}

sub create_meme {
    my ($args) = @_;

    my $meme = $args->{'meme'} or die "meme was not specified";
    my $original_top  = $args->{'top'};
    my $original_bottom = $args->{'bottom'};
    my $output = $args->{'output'} or die "no output was specified";

    my $top = convert_text($original_top);
    my $bottom = convert_text($original_bottom);

    my $meme_template = get_meme_template($meme);

    INFO "Reading template `$meme_template`";
    my $image = Image::Magick->new;
    $image->Read($meme_template);

    my ($width, $height) = $image->Get('width', 'height');

    INFO "Width is $width, height is $height";

    my $top_pointsize = (defined($top) && get_best_pointsize($image, $top));
    my $bottom_pointsize = (defined($bottom) && get_best_pointsize($image, $bottom));

    if (defined($top_pointsize)
        && defined($bottom_pointsize)
        && $top_pointsize != $bottom_pointsize
        && abs($top_pointsize - $bottom_pointsize) < 6) {
        if ($bottom_pointsize < $top_pointsize) {
            $top_pointsize = $bottom_pointsize;
        } else {
            $bottom_pointsize = $top_pointsize;
        }
    }

    if (defined($top)) {
        INFO "Writing `$original_top` with point size $top_pointsize";
        $image->Annotate(get_annotate_args($top, $top_pointsize, $width/2,
                                           $EDGE_SIZE + get_text_first_line_height($image, $top, $top_pointsize)));
    }

    if (defined($bottom)) {
        INFO "Writing `$original_bottom` with point size $bottom_pointsize";
        $image->Annotate(get_annotate_args($bottom, $bottom_pointsize, $width/2,
                                           $height-$EDGE_SIZE-
                                               (get_text_height($image, $bottom, $bottom_pointsize)
                                                - get_text_first_line_height($image, $bottom, $bottom_pointsize))));
    }

    INFO "Generating `$output`";
    $image->Write($output);
}

sub convert_text {
    my ($original_text) = @_;

    $original_text =~ s{/}{\n}g;

    return $original_text;
}

sub get_best_pointsize {
    my ($image, $text) = @_;

    my ($width, $height) = $image->Get('width', 'height');

    my $pointsize = 100;

    while (get_text_width($image, $text, $pointsize) > $width - 2 * $EDGE_SIZE
           || get_text_height($image, $text, $pointsize) > $height / 6) {
        --$pointsize;
    }

    return $pointsize;
}

sub get_text_width {
    my ($image, $text, $pointsize) = @_;

    return get_text_metric($image, $text, $pointsize, 4);
}

sub get_text_first_line_height {
    my ($image, $text, $pointsize) = @_;

    return get_text_metric($image, $text, $pointsize, 10) - get_text_metric($image, $text, $pointsize, 8);
}

sub get_text_height {
    my ($image, $text, $pointsize) = @_;

    return get_text_metric($image, $text, $pointsize, 5);
}



sub get_text_metric {
    my ($image, $text, $pointsize, $index) = @_;

    my ($width, $height) = $image->Get('width', 'height');

    return ($image->QueryMultilineFontMetrics(get_annotate_args($text, $pointsize, $width/2, $height/2)))[$index];
}

sub get_annotate_args {
    my ($text, $pointsize, $x, $y) = @_;

    return
        (text => $text,
         font => "Impact",
         pointsize=>$pointsize,
         fill=>"white",
         antialias=>"true",
         x => $x,
         y => $y,
         stroke => 'black',
         align => "Center");
}

sub get_meme_template {
    my ($meme) = @_;

    if (!check_cache($meme)) {
        INFO "Download '$meme'";
        download_meme_template($meme);
    }

    if (!check_cache($meme)) {
        die "cannot download `$meme`";
    }

    return cache_name($meme);
}

sub download_meme_template {
    my ($meme) = @_;

    my $url = get_url($meme);
    my $file = cache_name($meme);

    INFO "Download URL `$url` as `$file`";
    getstore($url, $file);
}

sub get_url {
    my ($meme) = @_;

    if ($meme eq 'idiot-nerd-girl') {
        return 'https://s3.amazonaws.com/kym-assets/photos/images/newsfeed/000/178/638/enhanced-buzz-20188-1309988258-10.jpg?1317011006'
    }

    return get_imgflip_url($meme);
}

sub get_imgflip_url {
    my ($meme) = @_;

    return "https://imgflip.com/s/meme/${meme}.jpg"
}

sub check_cache {
    my ($meme) = @_;

    return -r cache_name($meme);
}

sub cache_name {
    my ($meme) = @_;

    return "${CACHE_DIRECTORY}/${meme}.jpg";
}

#my @fonts = $image->QueryFont();
#print join("\n", @fonts),"\n";
